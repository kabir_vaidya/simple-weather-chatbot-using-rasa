from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals


from typing import Dict, Text, Any, List, Union, Optional

from rasa_sdk import Action
from rasa_sdk import Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction
from rasa_sdk.executor import CollectingDispatcher

class ActionTemperature(Action):

    def name(self):
        return 'action_temperature'
    
    def run(self, dispatcher, tracker, domain):
        import requests
        api_key = '886f3807f631c02e60489fec70bdc78d'
        loc = tracker.get_slot('location')

        params = {
        'access_key': api_key,
        'query': loc
        }

        api_result = requests.get('http://api.weatherstack.com/current', params)

        api_response = api_result.json()
        temperature = api_response['current']['temperature']

        message = "The temperature in {} is {} C".format(loc, temperature)

        dispatcher.utter_message(message)

        return [SlotSet('temperature', temperature)]
    
class LocationForm(FormAction):

    def name(self):
        return 'location_form'
    
    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        return ['location']
    
    def slot_mappings(self):
        return {
            'location': self.from_entity('location', intent=['inform_location', 'ask_temp'])
        }

    def submit(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any] ) -> List[Dict]:
      dispatcher.utter_message(template="utter_submit")
      return []