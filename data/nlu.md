## intent:greet
- hey
- hi
- hello
- heya
- hey there

## intent:affirm
- yes
- yeah
- yup
- that's right
- correct
- thanks
- thank you

## intent:deny
- no
- nope
- wrong
- nah
- that's not right
- no thanks

## intent:ask_temp
- what's the temperature
- how hot is it
- how cold is it
- temperature
- temp
- what is temp
- What's the temperature

## intent:inform_location
- [Chennai](location)
- I live in [Delhi](location)
- the location is [Mumbai](location)
- I stay at [Hyderabad](location)
- [Delhi](location)
- [Berlin](location)

## intent:goodbye
- bye
- goodbye
- goodnight
- see you later
- bubye
- bye bye
