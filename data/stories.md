## story_1
* greet
    - utter_greet
* ask_temp
    - location_form
    - form{"name":"location_form"}
    - form{"name":null}
    - action_temperature
    > check_response

## story_1
* ask_temp
    - location_form
    - form{"name":"location_form"}
    - form{"name":null}
    - action_temperature
    > check_response

## happy response
> check_response
* affirm
    - utter_happy
    > check_handled_happy

## sad response
> check_response
* deny
    - utter_sad
    > check_handled_sad

## goodbye
> check_handled_happy
> check_handled_sad
* goodbye
    - utter_goodbye
    - action_restart

